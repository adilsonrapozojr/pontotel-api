import falcon
from falcon import testing
import pytest
import urllib
import json

from myapi.app import api

@pytest.fixture
def client():
    return testing.TestClient(api)

def test_app(client):

    ex = '''
            {
                "domain": "localhost",
                "pages": {
                    "/mysite/": 2,
                    "/mysite/pagina1.html": 2,
                    "/mysite/pagina2.html": 2
                },
                "word": "MaGnA"
            }
    '''

    ex2 = '''
            {
                "domain": "localhost",
                "pages": {
                    "/mysite/pagina4.html": 7,
                    "/mysite/": 7
                },
                "word": "lorem"
            }
    '''

    assert_get(client, 'MaGnA', 'http://localhost/mysite/', ex)
    assert_get(client, 'lorem', 'http://localhost/mysite/pagina4.html', ex2)

def assert_get(client, word, site, expected_json):
    response = client.simulate_get('/wordcount', params={'w':word,'s':urllib.quote_plus(site)})
    assert response.status == falcon.HTTP_OK
    assert response.json == json.loads(expected_json)
