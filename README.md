# Exercício: Crawler + API #

O exercício consiste em desenvolver uma API que receba dois parametros:
1. Uma url de um site qualquer
2. Uma palavra qualquer

A API deve ser capaz de fazer um crawler no site informado e retornar uma resposta contendo um json com a quantidade de ocorrências da palavra informada.

-------------------

# Observações: #

- Crawler navega por default até por 2 níveis;
- As palavras consideradas são somente aquelas EXIBIDAS na página;
- A contagem -1 ocorre quando não foi possível acessar a página
- Modelo JSON:

### USAGE ###

/wordcount?w={palavra}&s={site com protocolo}

### INPUT ###

localhost:8000/wordcount?w=google&s=https://google.com/

### OUTPUT ###


```
#!JSON

{
    "domain": "google.com", 
    "pages": {
        "/": 3, 
        "/advanced_search": 4, 
        "/intl/pt-BR/about.html": 14, 
        "/intl/pt-BR/ads/": 14, 
        "/intl/pt-BR/policies/privacy/": 122, 
        "/intl/pt-BR/policies/terms/": 48, 
        "/language_tools": -1, 
        "/preferences": 7, 
        "/services/": 22
    }, 
    "word": "google"
}
```