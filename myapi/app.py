import falcon
from .wordcount import WordCount

api = application = falcon.API()

wordcount = WordCount()
api.add_route('/wordcount', wordcount)
