import json
import falcon
from mycrawler.crawler import Crawler

class WordCount(object):

    def on_get(self, req, resp):

        word = req.get_param('w',required=True)
        site = req.get_param('s',required=True)

        crawler = Crawler()
        doc = crawler.crawl(word, site)

        resp.body = json.dumps(doc, ensure_ascii=False)
