# -*- coding: utf-8 -*-

import re
from bs4 import BeautifulSoup
import urllib2
from urlparse import urlparse

def get_local_links(html, domain):
    hrefs = set()
    soup = BeautifulSoup(html, 'html.parser')
    a_tags = soup.findAll("a", href=True)
    for a_tag in a_tags:
        href = a_tag['href']
        u_parse = urlparse(href)
        if href.startswith('/'):
            hrefs.add(u_parse.path)
        elif u_parse.netloc == domain:
            hrefs.add(u_parse.path)
    return hrefs

def visible(element):
    if element.parent.name in ['style', 'script', '[document]', 'head', 'title']:
        return False
    elif re.match('<!--.*-->', unicode(element)):
        return False
    return True

class Crawler(object):
    def __init__(self, depth=2):
        self.depth = depth
        self.pages = {}

    def crawl(self, word, url):
        u_parse = urlparse(url)
        self.domain = u_parse.netloc
        self.scheme = u_parse.scheme
        self._crawl(word, [u_parse.path], self.depth)
        return self.build_json(word)

    def build_json(self, word):
        f_json = {}
        f_json['word'] = word
        f_json['domain'] = self.domain
        f_json['pages'] = self.pages
        return f_json

    def _crawl(self, word, urls, max_depth):
        n_urls = set()
        if max_depth:
            for url in urls:
                if url not in self.pages:
                    html = self.get_html(url)
                    counter = self.word_count(word, html)
                    self.pages[url] = counter
                    if (max_depth-1):
                        n_urls = n_urls.union(get_local_links(html, self.domain))
                        self._crawl(word, n_urls, max_depth-1)

    def get_html(self, url):
        try:
                req = urllib2.Request('%s://%s%s' % (self.scheme, self.domain, url))
                response = urllib2.urlopen(req)
                return response.read().decode('latin1', 'ignore')
        except urllib2.HTTPError, e:
            return ''

    def word_count(self, m_word, html_text):
        if not html_text:
            return -1
        soup = BeautifulSoup(html_text, 'html.parser')
        texts = soup.findAll(text=True)
        visible_texts = filter(visible, texts)

        words = []
        for text in visible_texts:
            words.extend(text.lower().split())

        m_word = m_word.decode('utf-8')
        r = re.compile(m_word.lower())
        m_words = []
        m_words = filter(r.search, words)
        return len(m_words)
